const express = require('express');
const fs = require('fs');
const path = "./messages";
const router = express.Router();

router.get('/', (req, res) => {
    try {
        const newMessage = [];
        const files = fs.readdirSync(path);
        const count = files.length < 5 ? files.length : 5;
        files.reverse();
        for(let i = 0; i < count; i++) {
            const message = fs.readFileSync(`${path}/${files[i]}`);
            newMessage.push(JSON.parse(message));
        }
        res.send(newMessage);
    } catch (e) {
        console.error(e)
    }
});

router.post('/', (req, res) => {
    try {
        const response = {
            message: req.body.message,
            datetime: new Date().toISOString()
        };

        console.log(JSON.stringify(response));

        fs.writeFileSync(`./messages/${response.datetime}.txt`, JSON.stringify(response));
        res.send(response);
    } catch (e) {
        console.log(e);
    }
});

module.exports = router;